#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <systemd/sd-bus.h>

static int method_authorize_push(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	/* Reply with the response */
	/* Returns the path to which the received object will be stored */
	printf ("AuthorizePush called");
	return sd_bus_reply_method_return(m, "s", "dummy.txt");
}

static int method_release(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	printf ("Release called");
	return sd_bus_reply_method_return(m, "");
}

static int method_cancel(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
	return sd_bus_reply_method_return(m, "");
}

/* The vtable of our little object, implements the net.poettering.Calculator interface */
static const sd_bus_vtable obex_agent_vtable[] = {
	SD_BUS_VTABLE_START(0),
	SD_BUS_METHOD("AuthorizePush",  "o", "s", method_authorize_push, SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("Release",        "",  "",  method_release,        SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_METHOD("Cancel",         "",  "",  method_cancel,         SD_BUS_VTABLE_UNPRIVILEGED),
	SD_BUS_VTABLE_END
};

int main(int argc, char *argv[]) {
	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message *m = NULL;
	sd_bus_slot *slot = NULL;
	sd_bus *bus = NULL;
	int r;

	/* Connect to the user bus */
	r = sd_bus_open_user(&bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to session bus: %s\n", strerror(-r));
		goto finish;
	}

	/* Install the object */
	r = sd_bus_add_object_vtable(bus,
		&slot,
		"/server/agent",  /* object path */
		"org.bluez.obex.Agent1",   /* interface name */
		obex_agent_vtable,
		NULL);
		if (r < 0) {
		fprintf(stderr, "Failed to create object table: %s\n", strerror(-r));
		goto finish;
	}

	/* Register the agent */
	r = sd_bus_call_method(bus,
		"org.bluez.obex",         /* service to contact */
		"/org/bluez/obex",        /* object path */
		"org.bluez.obex.AgentManager1", /* interface name */
		"RegisterAgent",          /* method name */
		&error,                   /* object to return error in */
		&m,                       /* return message on success */
		"o",                      /* input signature */
		"/server/agent");
	if (r < 0) {
		fprintf(stderr, "Failed to register agent: %s\n", error.message);
		goto finish;
	}

	for (;;) {
		/* Process requests */
		r = sd_bus_process(bus, NULL);
		if (r < 0) {
			fprintf(stderr, "Failed to process bus: %s\n", strerror(-r));
			goto finish;
		}
		if (r > 0) /* we processed a request, try to process another one, right-away */
			continue;

		/* Wait for the next request to process */
		r = sd_bus_wait(bus, (uint64_t) -1);
		if (r < 0) {
			fprintf(stderr, "Failed to wait on bus: %s\n", strerror(-r));
			goto finish;
		}
	}

	finish:
	sd_bus_error_free(&error);
	sd_bus_message_unref(m);

	sd_bus_slot_unref(slot);
	sd_bus_unref(bus);

	return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
