#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <systemd/sd-bus.h>

int main(int argc, char *argv[]) {
	sd_bus_error error = SD_BUS_ERROR_NULL;
	sd_bus_message *m = NULL;
	sd_bus *bus = NULL;
	int r;

	/* Connect to the system bus */
	r = sd_bus_open_system(&bus);
	if (r < 0) {
		fprintf(stderr, "Failed to connect to system bus: %s\n", strerror(-r));
		goto finish;
	}

	/* Issue the method call and store the respons message in m */
	r = sd_bus_call_method(bus,
						   "org.bluez",              /* service to contact */
						   "/org/bluez/hci0",        /* object path */
						   "org.bluez.Adapter1",     /* interface name */
						   "StartDiscovery",         /* method name */
						   &error,                   /* object to return error in */
						   &m,                       /* return message on success */
						   "");                      /* n */
	if (r < 0) {
		fprintf(stderr, "Failed to issue method call: %s\n", error.message);
		goto finish;
	}

	/* Display result as long as the user wants it */

	while (fgetc(stdin)) {
		usleep (1000);
	}

finish:
	sd_bus_error_free(&error);
	sd_bus_message_unref(m);
	sd_bus_unref(bus);

	return r < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
}
